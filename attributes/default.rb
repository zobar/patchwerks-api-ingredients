#
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

Ingredients.for_cookbook :patchwerks_api do
  data_bag_item 'services', 'patchwerks_api'

  attribute :environment, default: 'production'

  namespace :amqp do
    attribute :auth_mechanism, default: 'PLAIN'
    attribute :host,           default: 'localhost'
    attribute :port,           default: 5672
    attribute :vhost,          default: 'patchwerks_api'

    data_bag_attribute :cert_chain
    data_bag_attribute :private_key

    def ssl?
      !(cert_chain.nil? && private_key.nil?)
    end
  end

  namespace :cache do
    attribute :directory, default: 'patchwerks'
    attribute :prefix,    default: 'tiger/'
    attribute :provider,  default: 'aws'
  end

  namespace :database do
    attribute :database, default: 'patchwerks_api'
    attribute :host,     default: 'localhost'
    attribute :username, default: 'patchwerks_api'

    data_bag_attribute :client_certificate
    data_bag_attribute :client_key
    data_bag_attribute :root_certificate
  end

  namespace :fog do
    data_bag_attribute :aws_access_key_id
    data_bag_attribute :aws_secret_access_key
  end

  namespace :unicorn do
    attribute :listen, default: '0.0.0.0:80'
  end
end

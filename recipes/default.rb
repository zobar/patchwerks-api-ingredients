#
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

include_recipe 'ingredients'

ENV['LC_ALL'] = 'en_US.utf8'

directories = %w[
  /etc/patchwerks_api
  /srv/patchwerks_api
  /srv/patchwerks_api/shared
  /srv/patchwerks_api/shared/tmp
  /var/log/patchwerks_api
]

links = {
  '/srv/patchwerks_api/.postgresql'   => '/etc/patchwerks_api',
  '/srv/patchwerks_api/shared/config' => '/etc/patchwerks_api',
  '/srv/patchwerks_api/shared/log'    => '/var/log/patchwerks_api'
}

packages = %w[
  build-essential
  git
  libpq-dev
  libxslt1-dev
  libyaml-dev
  logrotate
  ruby1.9.1-dev
]

packages.each do |p|
  package p
end

gem_package 'bundler' do
  gem_binary 'gem1.9.1'
end

group 'patchwerks_api' do
  system true
end

user 'patchwerks_api' do
  gid    'patchwerks_api'
  home   '/srv/patchwerks_api'
  system true
end

directories.each do |d|
  directory d do
    group 'patchwerks_api'
    user  'patchwerks_api'
  end
end

links.each do |f, l|
  link f do
    to l
  end
end

file '/etc/patchwerks_api/amqp.crt' do
  not_if {patchwerks_api.amqp.cert_chain.nil?}

  content patchwerks_api.amqp.cert_chain
  group   'patchwerks_api'
  mode    0644
  owner   'patchwerks_api'
end

file '/etc/patchwerks_api/amqp.key' do
  not_if {patchwerks_api.amqp.private_key.nil?}

  content patchwerks_api.amqp.private_key
  group   'patchwerks_api'
  mode    0600
  owner   'patchwerks_api'
end

template '/etc/patchwerks_api/database.yml' do
  group     'patchwerks_api'
  mode      0644
  owner     'patchwerks_api'
  source    'database.yml.erb'
  variables database: patchwerks_api.database
end

template '/etc/patchwerks_api/fog.yml' do
  group     'patchwerks_api'
  mode      0644
  owner     'patchwerks_api'
  source    'fog.yml.erb'
  variables fog: patchwerks_api.fog
end

template '/etc/patchwerks_api/patchwerks.yml' do
  group     'patchwerks_api'
  mode      0644
  owner     'patchwerks_api'
  source    'patchwerks.yml.erb'
  variables amqp: patchwerks_api.amqp, cache: patchwerks_api.cache
end

file '/etc/patchwerks_api/postgresql.crt' do
  not_if {patchwerks_api.database.client_certificate.nil?}

  content patchwerks_api.database.client_certificate
  group   'patchwerks_api'
  mode    0644
  owner   'patchwerks_api'
end

file '/etc/patchwerks_api/postgresql.key' do
  not_if {patchwerks_api.database.client_key.nil?}

  content patchwerks_api.database.client_key
  group   'patchwerks_api'
  mode    0600
  owner   'patchwerks_api'
end

file '/etc/patchwerks_api/root.crt' do
  not_if {patchwerks_api.database.root_certificate.nil?}

  content patchwerks_api.database.root_certificate
  group   'patchwerks_api'
  mode    0644
  owner   'patchwerks_api'
end

template '/etc/patchwerks_api/unicorn.rb' do
  group     'patchwerks_api'
  mode      0644
  owner     'patchwerks_api'
  source    'unicorn.rb.erb'
  variables unicorn: patchwerks_api.unicorn
end

cookbook_file '/etc/profile.d/ruby1.9.1.sh' do
  mode   0755
  source 'ruby1.9.1.sh'
end

deploy_revision '/srv/patchwerks_api' do
  branch                 'chef-deploy'
  group                  'patchwerks_api'
  repo                   'git://github.com/zobar/patchwerks-api.git'
  symlink_before_migrate 'config/database.yml'   => 'config/database.yml',
                         'config/fog.yml'        => 'config/fog.yml',
                         'config/patchwerks.yml' => 'config/patchwerks.yml'
  user                   'patchwerks_api'

  before_migrate do
    execute 'bundle' do
      command     'bundle --binstubs --deployment --without development'
      cwd         release_path
      environment 'LC_ALL' => 'en_US.utf8',
                  'PATH'   => '/var/lib/gems/1.9.1/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
      group       'patchwerks_api'
      user        'patchwerks_api'
    end
  end
end
